package main

import (
	"fmt"
	"strconv"
	"strings"
)

var outPuts []string
var phaseSettings []string
var k int
var finalOutputs []int
var positions []int
var done bool
var firstLoop bool

var Abuffer string
var Bbuffer string
var Cbuffer string
var Dbuffer string
var Ebuffer string

func main() {

	var intcodes []string = make([]string, 5)
	phaseSettings = make([]string, 5)
	positions = make([]int, 5)

	for i := 0; i < 99999; i++ {
		phaseSettings = getPhaseSetting(i)
		if checkValidPhaseSetting(phaseSettings) {
			for i := 0; i < 5; i++ {
				intcodes[i] = "3,8,1001,8,10,8,105,1,0,0,21,46,55,68,89,110,191,272,353,434,99999,3,9,1002,9,3,9,1001,9,3,9,102,4,9,9,101,4,9,9,1002,9,5,9,4,9,99,3,9,102,3,9,9,4,9,99,3,9,1001,9,5,9,102,4,9,9,4,9,99,3,9,1001,9,5,9,1002,9,2,9,1001,9,5,9,1002,9,3,9,4,9,99,3,9,101,3,9,9,102,3,9,9,101,3,9,9,1002,9,4,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,99,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,99,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,99,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,99"
				positions[i] = 0
			}
			Abuffer = phaseSettings[0]
			Abuffer += ",0"
			Bbuffer = phaseSettings[1]
			Cbuffer = phaseSettings[2]
			Dbuffer = phaseSettings[3]
			Ebuffer = phaseSettings[4]

			done = false
			outPuts = make([]string, 5)
			firstLoop = true
			for !done {
				for k = 0; k <= 4; k++ {

					intcode := strings.Split(intcodes[k], ",")

					//fmt.Println(fmt.Sprintf("noun[%d] and verb[%d]", noun, verb))
					intcodes[k] = strings.Join(processIntcode(intcode), ",")
					if done {
						break
					}
					if k == 4 {
						firstLoop = false

					}
				}
			}
			fmt.Println("test")
		}
	}
	highest := 0
	for i := 0; i < len(finalOutputs); i++ {
		if finalOutputs[i] > highest {
			highest = finalOutputs[i]
		}
	}
	fmt.Printf("Highetst [%d]", highest)

}

func checkValidPhaseSetting(settings []string) bool {
	for i := 0; i < len(settings); i++ {
		digit1, _ := strconv.Atoi(settings[i])
		if digit1 < 5 {
			return false
		}
		for j := 0; j < len(settings); j++ {
			digit2, _ := strconv.Atoi(settings[j])
			if digit2 < 5 {
				return false
			}
			if j != i {
				if digit1 == digit2 {
					return false
				}
			}
		}
	}
	return true
}

func getPhaseSetting(i int) []string {
	str := strconv.Itoa(i)
	strplit := strings.Split(str, "")
	phaseSettings := make([]string, 5)
	for i := 0; i < 5; i++ {
		if i < len(strplit) {
			phaseSettings[4-i] = strplit[i]
		} else {
			phaseSettings[4-i] = "0"
		}
	}
	return phaseSettings
}

func processIntcode(intcode []string) []string {
	// 99 halt
	// 1 add together
	// 2 multiply
	// first 2 = input
	// 3rd = output
	position := positions[k]
	opcode := 99999
	if len(intcode[position]) > 1 {
		opcode, _ = strconv.Atoi(intcode[position][0:1])
	} else {
		opcode, _ = strconv.Atoi(intcode[position])
	}

	for opcode != 99 && position < len(intcode) {
		if len(intcode[position]) > 1 {
			if intcode[position][0:2] == "99" {
				if k == 4 {
					finalOutput, _ := strconv.Atoi(outPuts[4])
					finalOutputs = append(finalOutputs, finalOutput)
					done = true
				}

				position += 2
				break
			}
			opcodeLen := len(intcode[position])
			opcode, _ = strconv.Atoi(intcode[position][opcodeLen-1 : opcodeLen])
		} else {
			opcode, _ = strconv.Atoi(intcode[position])
		}
		var paramModes []int = make([]int, 10)
		opcodeLen := len(intcode[position])
		// 1001 = 0 1
		for j := 0; j > len(paramModes); j++ {
			paramModes[j] = 0
		}
		for i := 0; i < opcodeLen-2; i++ {
			paramModes[opcodeLen-i-3], _ = strconv.Atoi(intcode[position][i : i+1])
			if paramModes[i] > 1 {
				paramModes[i] = 0
			}

		}
		//fmt.Println(fmt.Sprintf("opcode[%d] input1[%d] input2[%d] output[%d]", opcode, input1, input2,  output))
		if opcode == 1 {
			processOpcode1(intcode, position, paramModes)
			position += 4
		}
		if opcode == 2 {
			processOpcode2(intcode, position, paramModes)
			position += 4
		}
		if opcode == 3 {
			processOpcode3(intcode, position, paramModes)
			position += 2
		}
		if opcode == 4 {
			processOpcode4(intcode, position, paramModes)
			position += 2
			positions[k] = position
			break
		}
		if opcode == 5 {
			position = processOpcode5(intcode, position, paramModes)
		}
		if opcode == 6 {
			position = processOpcode6(intcode, position, paramModes)
		}
		if opcode == 7 {
			processOpcode7(intcode, position, paramModes)
			position += 4
		}
		if opcode == 8 {
			processOpcode8(intcode, position, paramModes)
			position += 4
		}

	}
	return intcode
}

func processOpcode8(intcode []string, position int, paramModes []int) {
	inputValue1 := -1
	if paramModes[0] == 0 {
		pos, _ := strconv.Atoi(intcode[position+1])
		inputValue1, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue1, _ = strconv.Atoi(intcode[position+1])
	}
	inputValue2 := -1
	if paramModes[1] == 0 {
		pos, _ := strconv.Atoi(intcode[position+2])
		inputValue2, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue2, _ = strconv.Atoi(intcode[position+2])
	}
	output, _ := strconv.Atoi(intcode[position+3])
	if inputValue1 == inputValue2 {
		intcode[output] = strconv.Itoa(1)
	} else {
		intcode[output] = strconv.Itoa(0)
	}
}

func processOpcode7(intcode []string, position int, paramModes []int) {
	inputValue1 := -1
	if paramModes[0] == 0 {
		pos, _ := strconv.Atoi(intcode[position+1])
		inputValue1, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue1, _ = strconv.Atoi(intcode[position+1])
	}
	inputValue2 := -1
	if paramModes[1] == 0 {
		pos, _ := strconv.Atoi(intcode[position+2])
		inputValue2, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue2, _ = strconv.Atoi(intcode[position+2])
	}
	output, _ := strconv.Atoi(intcode[position+3])
	if inputValue1 < inputValue2 {
		intcode[output] = strconv.Itoa(1)
	} else {
		intcode[output] = strconv.Itoa(0)
	}

}

func processOpcode6(intcode []string, position int, paramModes []int) int {
	inputValue1 := -1
	if paramModes[0] == 0 {
		pos, _ := strconv.Atoi(intcode[position+1])
		inputValue1, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue1, _ = strconv.Atoi(intcode[position+1])
	}
	inputValue2 := -1
	if paramModes[1] == 0 {
		pos, _ := strconv.Atoi(intcode[position+2])
		inputValue2, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue2, _ = strconv.Atoi(intcode[position+2])
	}
	if inputValue1 == 0 {
		return inputValue2
	} else {
		return position + 3
	}

}

func processOpcode5(intcode []string, position int, paramModes []int) int {
	inputValue1 := -1
	if paramModes[0] == 0 {
		pos, _ := strconv.Atoi(intcode[position+1])
		inputValue1, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue1, _ = strconv.Atoi(intcode[position+1])
	}
	inputValue2 := -1
	if paramModes[1] == 0 {
		pos, _ := strconv.Atoi(intcode[position+2])
		inputValue2, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue2, _ = strconv.Atoi(intcode[position+2])
	}
	if inputValue1 != 0 {
		return inputValue2
	} else {
		return position + 3
	}

}

func processOpcode4(intcode []string, position int, paramModes []int) {
	inputValue1 := -1
	if paramModes[0] == 0 {
		pos, _ := strconv.Atoi(intcode[position+1])
		inputValue1, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue1, _ = strconv.Atoi(intcode[position+1])
	}
	if k == 0 {
		str := strconv.Itoa(inputValue1)
		if Bbuffer == "" {
			Bbuffer = str
		} else {
			Bbuffer += "," + str
		}

	}
	if k == 1 {
		str := strconv.Itoa(inputValue1)
		if Cbuffer == "" {
			Cbuffer = str
		} else {
			Cbuffer += "," + str
		}
	}
	if k == 2 {
		str := strconv.Itoa(inputValue1)
		if Dbuffer == "" {
			Dbuffer = str
		} else {
			Dbuffer += "," + str
		}
	}
	if k == 3 {
		str := strconv.Itoa(inputValue1)
		if Ebuffer == "" {
			Ebuffer = str
		} else {
			Ebuffer += "," + str
		}
	}
	if k == 4 {
		str := strconv.Itoa(inputValue1)
		if Abuffer == "" {
			Abuffer = str
		} else {
			Abuffer += "," + str
		}
	}
	outPuts[k] = strconv.Itoa(inputValue1)
}

func processOpcode3(intcode []string, position int, paramModes []int) {
	//fmt.Print("Enter text: ")
	//var input string
	//fmt.Scanln(&input)
	pos, _ := strconv.Atoi(intcode[position+1])
	if k == 0 {
		bufSplit := strings.Split(Abuffer, ",")

		intcode[pos] = bufSplit[0]
		Abuffer = strings.Join(bufSplit[1:], ",")
	}
	if k == 1 {
		bufSplit := strings.Split(Bbuffer, ",")

		intcode[pos] = bufSplit[0]
		Bbuffer = strings.Join(bufSplit[1:], ",")
	}
	if k == 2 {
		bufSplit := strings.Split(Cbuffer, ",")

		intcode[pos] = bufSplit[0]
		Cbuffer = strings.Join(bufSplit[1:], ",")
	}
	if k == 3 {
		bufSplit := strings.Split(Dbuffer, ",")

		intcode[pos] = bufSplit[0]
		Dbuffer = strings.Join(bufSplit[1:], ",")
	}
	if k == 4 {
		bufSplit := strings.Split(Ebuffer, ",")

		intcode[pos] = bufSplit[0]
		Ebuffer = strings.Join(bufSplit[1:], ",")
	}
	//intcode[pos] = "5"
}

func processOpcode2(intcode []string, position int, paramModes []int) {
	inputValue1 := -1
	if paramModes[0] == 0 {
		pos, _ := strconv.Atoi(intcode[position+1])
		inputValue1, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue1, _ = strconv.Atoi(intcode[position+1])
	}
	inputValue2 := -1
	if paramModes[1] == 0 {
		pos, _ := strconv.Atoi(intcode[position+2])
		inputValue2, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue2, _ = strconv.Atoi(intcode[position+2])
	}
	output, _ := strconv.Atoi(intcode[position+3])
	intcode[output] = strconv.Itoa(inputValue1 * inputValue2)
}

func processOpcode1(intcode []string, position int, paramModes []int) {
	inputValue1 := -1
	if paramModes[0] == 0 {
		pos, _ := strconv.Atoi(intcode[position+1])
		inputValue1, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue1, _ = strconv.Atoi(intcode[position+1])
	}
	inputValue2 := -1
	if paramModes[1] == 0 {
		pos, _ := strconv.Atoi(intcode[position+2])
		inputValue2, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue2, _ = strconv.Atoi(intcode[position+2])
	}
	output, _ := strconv.Atoi(intcode[position+3])
	intcode[output] = strconv.Itoa(inputValue1 + inputValue2)
}

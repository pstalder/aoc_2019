package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	matchCount := 0
	min := 136760
	max := 595730

	for i := min; i < max; i++ {
		if checkValidPass(i) {
			matchCount++
		}
	}

	fmt.Println(fmt.Sprintf("Match count [%d]", matchCount))
}

func checkValidPass(i int) bool {
	pwStr := strconv.Itoa(i)
	valid := true

	if !checkAdjacentDouble(pwStr) {
		valid = false
	}
	if !checkAscending(pwStr) {
		valid = false
	}

	return valid
}

func checkAscending(str string) bool {
	valid := false
	arr := strings.Split(str, "")
	previous := -1
	for i := 0; i < len(arr); i++ {
		current, _ := strconv.Atoi(arr[i])
		if current >= previous {
			valid = true
		} else {
			valid = false
			break
		}
		previous = current
	}
	return valid
}

func checkAdjacentDouble(str string) bool {
	valid := false
	arr := strings.Split(str, "")
	previous := "00"
	next := "123213"
	for i := 1; i < len(arr); i++ {
		currentFirst := arr[i-1]
		currentSecond := arr[i]
		if i > 1 {
			previous = arr[i-2]
		}
		if i+1 < len(arr) {
			next = arr[i+1]
		} else {
			next = "12312"
		}

		if currentFirst == currentSecond && currentFirst != previous && currentFirst != next {
			valid = true
		}
	}
	return valid
}

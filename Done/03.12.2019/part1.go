package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var arraySize int = 40000

func main() {
	var wireMap [][]string = drawWireMap("C:/data/workspace/aoc_2019/active/input")
	closestDistance := 1000000
	for x := 0; x < arraySize-1; x++ {
		for y := 0; y < arraySize-1; y++ {
			if wireMap[x][y][0:1] == "x" {
				distance, _ := strconv.Atoi(wireMap[x][y][1:])
				if distance < closestDistance {
					closestDistance = distance
				}
			}
		}
	}
	fmt.Println(fmt.Sprintf("Closest distance [%d]", closestDistance))
}

func calcDistance(x int, y int) int {
	distance := 0
	startX := arraySize / 2
	startY := arraySize / 2
	xDistance := 0
	yDistance := 0
	if x < startX {
		xDistance = startX - x
	} else {
		xDistance = x - startX
	}
	if y < startY {
		yDistance = startY - y
	} else {
		yDistance = y - startY
	}
	distance = xDistance + yDistance
	return distance
}
func drawWireMap(path string) [][]string {
	inFile, err := os.Open(path)
	if err != nil {
		fmt.Println(err.Error() + `: ` + path)
		return nil
	}
	defer inFile.Close()

	scanner := bufio.NewScanner(inFile)

	var wireMap [][]string = initializeMap()
	currentWire := 1
	for scanner.Scan() {
		inputLine := scanner.Text()
		inputArgs := strings.Split(inputLine, ",")
		wireMap = drawMapForWire(inputArgs, wireMap, currentWire)
		currentWire++
	}
	return wireMap
}

func initializeMap() [][]string {
	var wireMap [][]string = make([][]string, arraySize)
	for x := 0; x < arraySize; x++ {
		wireMap[x] = make([]string, arraySize)
		for y := 0; y < arraySize; y++ {
			wireMap[x][y] = "."
		}
	}
	return wireMap
}

func drawMapForWire(inputArgs []string, wireMap [][]string, currentWire int) [][]string {
	currentX := arraySize / 2
	currentY := arraySize / 2
	wireSteps := 0
	for i := 0; i < len(inputArgs); i++ {
		direction := inputArgs[i][0:1]
		amount, _ := strconv.Atoi(inputArgs[i][1:])
		for amount > 0 {
			wireSteps++
			switch direction {
			case "L":
				currentX--
				drawPosition(wireMap, currentX, currentY, currentWire, "-", wireSteps)
			case "R":
				currentX++
				drawPosition(wireMap, currentX, currentY, currentWire, "-", wireSteps)
			case "D":
				currentY--
				drawPosition(wireMap, currentX, currentY, currentWire, "|", wireSteps)
			case "U":
				currentY++
				drawPosition(wireMap, currentX, currentY, currentWire, "|", wireSteps)
			}
			amount--

		}
	}
	return wireMap
}

func drawPosition(wireMap [][]string, x int, y int, currentWire int, direction string, wireSteps int) {
	if wireMap[x][y] == "." {
		wireMap[x][y] = strconv.Itoa(currentWire) + direction + strconv.Itoa(wireSteps)
	} else {
		if wireMap[x][y][0:1] != strconv.Itoa(currentWire) {
			firstWireSteps, _ := strconv.Atoi(wireMap[x][y][2:])
			wireMap[x][y] = "x" + strconv.Itoa(firstWireSteps+wireSteps)
			fmt.Println(fmt.Sprintf("Found intersection at [%d][%d]", x, y))
		}
	}
}

package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	totalOrbits := getTotalOrbits("D:/workspace/aoc_2019/active/input")
	fmt.Printf("Total orbits [%d]", totalOrbits)
}

func getTotalOrbits(path string) int {
	inFile, err := os.Open(path)
	if err != nil {
		fmt.Println(err.Error() + `: ` + path)
		return 0
	}
	defer inFile.Close()

	scanner := bufio.NewScanner(inFile)
	var orbitMap map[string]string
	orbitMap = make(map[string]string)
	for scanner.Scan() {
		inputLine := scanner.Text()
		inputSplit := strings.Split(inputLine, ")")
		orbitMap[inputSplit[1]] = inputSplit[0]
	}

	/*totalOrbitCount := 0
	for k, v := range orbitMap {
		totalOrbitCount += followOrbits(k, orbitMap, 0)
		totalOrbitCount ++ // for direct orbit maybe off by one for COM
		fmt.Printf("key[%s] value[%s]\n", k, v)
	}*/

	var sanOrbits []string
	sanOrbits = followOrbits("SAN", orbitMap, sanOrbits)
	var youOrbits []string
	youOrbits = followOrbits("YOU", orbitMap, youOrbits)
	var commonOrbits []string

	for i, v1 := range sanOrbits {
		for j, v2 := range youOrbits {
			if v1 == v2 {
				return i + j
				commonOrbits = append(commonOrbits, v1)
			}
		}
	}
	/*firstCommonOrbit := commonOrbits[0]
	sanCounts := countOrbits(orbitMap["SAN"], orbitMap, 0, firstCommonOrbit)
	fmt.Println(sanCounts)
	youCounts := countOrbits(orbitMap["YOU"], orbitMap, 0, firstCommonOrbit)
	fmt.Println(youCounts)*/
	return 0
	//return sanCounts+youCounts
}

func followOrbits(k string, orbitMap map[string]string, orbits []string) []string {
	whatIorbit := orbitMap[k]
	if whatIorbit == "COM" {
		orbits = append(orbits, "COM")
		return orbits
	} else {
		orbits = append(orbits, whatIorbit)
		orbits = followOrbits(whatIorbit, orbitMap, orbits)
	}
	return orbits
}

func countOrbits(k string, orbitMap map[string]string, orbits int, exit string) int {
	whatIorbit := orbitMap[k]
	if whatIorbit == exit {
		return orbits
	} else {
		orbits += 1
		orbits = countOrbits(whatIorbit, orbitMap, orbits, exit)
	}
	return orbits
}

package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

//align warp drive
//measurements from fifty stars
//
// calculate fuel required
// fuel for each module is based on its mass
// mass / 3 round down subtract 2

// calculate total fuel required
func main() {
	totalFuelRequired := getTotalFuelRequired("D:/workspace/aoc_2019/active/input/input")
	fmt.Println(fmt.Sprintf("Total fuel required is [%f]", totalFuelRequired))
}

func getFuelRequired(mass float64) float64 {
	var requiredFuel float64
	requiredFuel = mass / 3
	requiredFuel = float64(int(requiredFuel))
	requiredFuel = requiredFuel - 2
	return requiredFuel
}

func getActualFuelRequired(mass float64) float64 {
	var actualFuelRequired float64 = 0
	nextRequiredFuel := getFuelRequired(mass)
	for nextRequiredFuel > 0 {
		actualFuelRequired += nextRequiredFuel
		nextRequiredFuel = getFuelRequired(nextRequiredFuel)
	}

	return actualFuelRequired
}

func getTotalFuelRequired(path string) float64 {
	inFile, err := os.Open(path)
	if err != nil {
		fmt.Println(err.Error() + `: ` + path)
		return 0
	}
	defer inFile.Close()

	scanner := bufio.NewScanner(inFile)
	var totalFuelRequired float64 = 0
	for scanner.Scan() {
		inputLine := scanner.Text()
		inputFloat, _ := strconv.ParseFloat(inputLine, 64)
		totalFuelRequired += getActualFuelRequired(inputFloat)
	}
	return totalFuelRequired
}

package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	noun := 0
	verb := 0
	for noun = 0; noun < 99; noun++ {
		for verb = 0; verb < 99; verb++ {
			intcode := strings.Split("1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,13,1,19,1,19,10,23,2,10,23,27,1,27,6,31,1,13,31,35,1,13,35,39,1,39,10,43,2,43,13,47,1,47,9,51,2,51,13,55,1,5,55,59,2,59,9,63,1,13,63,67,2,13,67,71,1,71,5,75,2,75,13,79,1,79,6,83,1,83,5,87,2,87,6,91,1,5,91,95,1,95,13,99,2,99,6,103,1,5,103,107,1,107,9,111,2,6,111,115,1,5,115,119,1,119,2,123,1,6,123,0,99,2,14,0,0", ",")
			intcode[1] = strconv.Itoa(noun)
			intcode[2] = strconv.Itoa(verb)
			//fmt.Println(fmt.Sprintf("noun[%d] and verb[%d]", noun, verb))
			intcode = processIntcode(intcode)
			if intcode[0] == "19690720" {
				fmt.Println(fmt.Sprintf("--------------------------------------noun[%d] and verb[%d]", noun, verb))
				break
			}
		}
	}

}

func processIntcode(intcode []string) []string {
	// 99 halt
	// 1 add together
	// 2 multiply
	// first 2 = input
	// 3rd = output
	position := 0
	opcode, _ := strconv.Atoi(intcode[position])
	for opcode != 99 && position+4 < len(intcode) {
		opcode, _ := strconv.Atoi(intcode[position])
		input1, _ := strconv.Atoi(intcode[position+1])
		input2, _ := strconv.Atoi(intcode[position+2])
		output, _ := strconv.Atoi(intcode[position+3])
		//fmt.Println(fmt.Sprintf("opcode[%d] input1[%d] input2[%d] output[%d]", opcode, input1, input2,  output))
		if opcode == 1 {
			processOpcode1(intcode, input1, input2, output)
		}
		if opcode == 2 {
			processOpcode2(intcode, input1, input2, output)
		}
		if opcode != 1 && opcode != 2 && opcode != 99 {
			return intcode
		}
		position += 4
	}
	return intcode
}

func processOpcode2(intcode []string, input1 int, input2 int, output int) {
	inputValue1, _ := strconv.Atoi(intcode[input1])
	inputValue2, _ := strconv.Atoi(intcode[input2])
	intcode[output] = strconv.Itoa(inputValue1 * inputValue2)
	//fmt.Println(fmt.Sprintf("Result[%s]", intcode[output]))
}

func processOpcode1(intcode []string, input1 int, input2 int, output int) {
	inputValue1, _ := strconv.Atoi(intcode[input1])
	inputValue2, _ := strconv.Atoi(intcode[input2])
	intcode[output] = strconv.Itoa(inputValue1 + inputValue2)
	//fmt.Println(fmt.Sprintf("Result[%s]", intcode[output]))
}

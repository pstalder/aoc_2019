package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	intcode := strings.Split("3,225,1,225,6,6,1100,1,238,225,104,0,1101,33,37,225,101,6,218,224,1001,224,-82,224,4,224,102,8,223,223,101,7,224,224,1,223,224,223,1102,87,62,225,1102,75,65,224,1001,224,-4875,224,4,224,1002,223,8,223,1001,224,5,224,1,224,223,223,1102,49,27,225,1101,6,9,225,2,69,118,224,101,-300,224,224,4,224,102,8,223,223,101,6,224,224,1,224,223,223,1101,76,37,224,1001,224,-113,224,4,224,1002,223,8,223,101,5,224,224,1,224,223,223,1101,47,50,225,102,43,165,224,1001,224,-473,224,4,224,102,8,223,223,1001,224,3,224,1,224,223,223,1002,39,86,224,101,-7482,224,224,4,224,102,8,223,223,1001,224,6,224,1,223,224,223,1102,11,82,225,1,213,65,224,1001,224,-102,224,4,224,1002,223,8,223,1001,224,6,224,1,224,223,223,1001,14,83,224,1001,224,-120,224,4,224,1002,223,8,223,101,1,224,224,1,223,224,223,1102,53,39,225,1101,65,76,225,4,223,99,0,0,0,677,0,0,0,0,0,0,0,0,0,0,0,1105,0,99999,1105,227,247,1105,1,99999,1005,227,99999,1005,0,256,1105,1,99999,1106,227,99999,1106,0,265,1105,1,99999,1006,0,99999,1006,227,274,1105,1,99999,1105,1,280,1105,1,99999,1,225,225,225,1101,294,0,0,105,1,0,1105,1,99999,1106,0,300,1105,1,99999,1,225,225,225,1101,314,0,0,106,0,0,1105,1,99999,1107,677,226,224,1002,223,2,223,1005,224,329,101,1,223,223,8,677,226,224,102,2,223,223,1006,224,344,1001,223,1,223,108,677,677,224,1002,223,2,223,1006,224,359,1001,223,1,223,1108,226,677,224,102,2,223,223,1006,224,374,1001,223,1,223,1008,677,226,224,102,2,223,223,1005,224,389,101,1,223,223,7,226,677,224,102,2,223,223,1005,224,404,1001,223,1,223,1007,677,677,224,1002,223,2,223,1006,224,419,101,1,223,223,107,677,226,224,102,2,223,223,1006,224,434,101,1,223,223,7,677,677,224,1002,223,2,223,1005,224,449,101,1,223,223,108,677,226,224,1002,223,2,223,1006,224,464,101,1,223,223,1008,226,226,224,1002,223,2,223,1006,224,479,101,1,223,223,107,677,677,224,1002,223,2,223,1006,224,494,1001,223,1,223,1108,677,226,224,102,2,223,223,1005,224,509,101,1,223,223,1007,226,677,224,102,2,223,223,1005,224,524,1001,223,1,223,1008,677,677,224,102,2,223,223,1005,224,539,1001,223,1,223,1107,677,677,224,1002,223,2,223,1006,224,554,1001,223,1,223,1007,226,226,224,1002,223,2,223,1005,224,569,1001,223,1,223,7,677,226,224,1002,223,2,223,1006,224,584,1001,223,1,223,108,226,226,224,102,2,223,223,1005,224,599,1001,223,1,223,8,677,677,224,102,2,223,223,1005,224,614,1001,223,1,223,1107,226,677,224,102,2,223,223,1005,224,629,1001,223,1,223,8,226,677,224,102,2,223,223,1006,224,644,1001,223,1,223,1108,226,226,224,1002,223,2,223,1006,224,659,101,1,223,223,107,226,226,224,1002,223,2,223,1006,224,674,1001,223,1,223,4,223,99,226", ",")
	//fmt.Println(fmt.Sprintf("noun[%d] and verb[%d]", noun, verb))
	intcode = processIntcode(intcode)

}

func processIntcode(intcode []string) []string {
	// 99 halt
	// 1 add together
	// 2 multiply
	// first 2 = input
	// 3rd = output
	position := 0
	opcode := 99999
	if len(intcode[position]) > 1 {
		opcode, _ = strconv.Atoi(intcode[position][0:1])
	} else {
		opcode, _ = strconv.Atoi(intcode[position])
	}

	for opcode != 99 && position < len(intcode) {
		if len(intcode[position]) > 1 {
			opcodeLen := len(intcode[position])
			opcode, _ = strconv.Atoi(intcode[position][opcodeLen-1 : opcodeLen])
		} else {
			opcode, _ = strconv.Atoi(intcode[position])
		}
		var paramModes []int = make([]int, 10)
		opcodeLen := len(intcode[position])
		// 1001 = 0 1
		for j := 0; j > len(paramModes); j++ {
			paramModes[j] = 0
		}
		for i := 0; i < opcodeLen-2; i++ {
			paramModes[opcodeLen-i-3], _ = strconv.Atoi(intcode[position][i : i+1])
			if paramModes[i] > 1 {
				paramModes[i] = 0
			}

		}
		//fmt.Println(fmt.Sprintf("opcode[%d] input1[%d] input2[%d] output[%d]", opcode, input1, input2,  output))
		if opcode == 1 {
			processOpcode1(intcode, position, paramModes)
			position += 4
		}
		if opcode == 2 {
			processOpcode2(intcode, position, paramModes)
			position += 4
		}
		if opcode == 3 {
			processOpcode3(intcode, position, paramModes)
			position += 2
		}
		if opcode == 4 {
			processOpcode4(intcode, position, paramModes)
			position += 2
		}
		if opcode == 5 {
			position = processOpcode5(intcode, position, paramModes)
		}
		if opcode == 6 {
			position = processOpcode6(intcode, position, paramModes)
		}
		if opcode == 7 {
			processOpcode7(intcode, position, paramModes)
			position += 4
		}
		if opcode == 8 {
			processOpcode8(intcode, position, paramModes)
			position += 4
		}

	}
	return intcode
}

func processOpcode8(intcode []string, position int, paramModes []int) {
	inputValue1 := -1
	if paramModes[0] == 0 {
		pos, _ := strconv.Atoi(intcode[position+1])
		inputValue1, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue1, _ = strconv.Atoi(intcode[position+1])
	}
	inputValue2 := -1
	if paramModes[1] == 0 {
		pos, _ := strconv.Atoi(intcode[position+2])
		inputValue2, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue2, _ = strconv.Atoi(intcode[position+2])
	}
	output, _ := strconv.Atoi(intcode[position+3])
	if inputValue1 == inputValue2 {
		intcode[output] = strconv.Itoa(1)
	} else {
		intcode[output] = strconv.Itoa(0)
	}
}

func processOpcode7(intcode []string, position int, paramModes []int) {
	inputValue1 := -1
	if paramModes[0] == 0 {
		pos, _ := strconv.Atoi(intcode[position+1])
		inputValue1, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue1, _ = strconv.Atoi(intcode[position+1])
	}
	inputValue2 := -1
	if paramModes[1] == 0 {
		pos, _ := strconv.Atoi(intcode[position+2])
		inputValue2, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue2, _ = strconv.Atoi(intcode[position+2])
	}
	output, _ := strconv.Atoi(intcode[position+3])
	if inputValue1 < inputValue2 {
		intcode[output] = strconv.Itoa(1)
	} else {
		intcode[output] = strconv.Itoa(0)
	}

}

func processOpcode6(intcode []string, position int, paramModes []int) int {
	inputValue1 := -1
	if paramModes[0] == 0 {
		pos, _ := strconv.Atoi(intcode[position+1])
		inputValue1, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue1, _ = strconv.Atoi(intcode[position+1])
	}
	inputValue2 := -1
	if paramModes[1] == 0 {
		pos, _ := strconv.Atoi(intcode[position+2])
		inputValue2, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue2, _ = strconv.Atoi(intcode[position+2])
	}
	if inputValue1 == 0 {
		return inputValue2
	} else {
		return position + 3
	}

}

func processOpcode5(intcode []string, position int, paramModes []int) int {
	inputValue1 := -1
	if paramModes[0] == 0 {
		pos, _ := strconv.Atoi(intcode[position+1])
		inputValue1, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue1, _ = strconv.Atoi(intcode[position+1])
	}
	inputValue2 := -1
	if paramModes[1] == 0 {
		pos, _ := strconv.Atoi(intcode[position+2])
		inputValue2, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue2, _ = strconv.Atoi(intcode[position+2])
	}
	if inputValue1 != 0 {
		return inputValue2
	} else {
		return position + 3
	}

}

func processOpcode4(intcode []string, position int, paramModes []int) {
	inputValue1 := -1
	if paramModes[0] == 0 {
		pos, _ := strconv.Atoi(intcode[position+1])
		inputValue1, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue1, _ = strconv.Atoi(intcode[position+1])
	}
	fmt.Println(inputValue1)
}

func processOpcode3(intcode []string, position int, paramModes []int) {
	//fmt.Print("Enter text: ")
	//var input string
	//fmt.Scanln(&input)
	pos, _ := strconv.Atoi(intcode[position+1])
	intcode[pos] = "5"
}

func processOpcode2(intcode []string, position int, paramModes []int) {
	inputValue1 := -1
	if paramModes[0] == 0 {
		pos, _ := strconv.Atoi(intcode[position+1])
		inputValue1, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue1, _ = strconv.Atoi(intcode[position+1])
	}
	inputValue2 := -1
	if paramModes[1] == 0 {
		pos, _ := strconv.Atoi(intcode[position+2])
		inputValue2, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue2, _ = strconv.Atoi(intcode[position+2])
	}
	output, _ := strconv.Atoi(intcode[position+3])
	intcode[output] = strconv.Itoa(inputValue1 * inputValue2)
}

func processOpcode1(intcode []string, position int, paramModes []int) {
	inputValue1 := -1
	if paramModes[0] == 0 {
		pos, _ := strconv.Atoi(intcode[position+1])
		inputValue1, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue1, _ = strconv.Atoi(intcode[position+1])
	}
	inputValue2 := -1
	if paramModes[1] == 0 {
		pos, _ := strconv.Atoi(intcode[position+2])
		inputValue2, _ = strconv.Atoi(intcode[pos])
	} else {
		inputValue2, _ = strconv.Atoi(intcode[position+2])
	}
	output, _ := strconv.Atoi(intcode[position+3])
	intcode[output] = strconv.Itoa(inputValue1 + inputValue2)
}
